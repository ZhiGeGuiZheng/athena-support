import Vue from 'vue'
import Router from 'vue-router'
import BaseIndex from '@/components/Base'
import Chat from '@/components/chat/Chat'
import Current from '@/components/chat/Current'
import Waiting from '@/components/chat/Waiting'
import Cancel from '@/components/chat/Cancel'
import Settings from '@/components/setting/Setting'
import Base from '@/components/setting/base/Base'
import Advance from '@/components/setting/base/Advance'
import MsgType from '@/components/setting/base/MsgType'
import Structure from '@/components/setting/Structure/Main'
import Workmate from '@/components/Workmate/Workmate'
import MateChat from '@/components/Workmate/MateChat'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/', redirect: '/chat/current',
            component: BaseIndex,
            children: [
                {
                    path: 'system', redirect: '/chat/current'
                },
                {
                    path: 'workmate',
                    component: Workmate,

                    children: [
                        {
                            path: ':id',
                            component: MateChat
                        },
                    ]
                },
                {
                    path: 'chat',
                    redirect: '/chat/current',
                    name: 'Chat',
                    component: Chat,
                    children: [
                        {path: 'current', name: 'current', component: Current},
                        {path: 'waiting', name: 'waiting', component: Waiting},
                        {path: 'cancel', name: 'cancel', component: Cancel},
                    ]
                },
                {
                    path: 'settings', name: 'settings', component: Settings, redirect: '/settings/basic',
                    children: [
                        {
                            path: 'basic', name: 'basic', component: Base
                        },
                        {
                            path: 'advance', name: 'advance', component: Advance
                        },
                        {
                            path: 'msgtype', name: 'msgtype', component: MsgType
                        },
                        {
                            path: 'structure', name: 'structure', component: Structure
                        }
                    ]
                },
            ]
        },
    ]
})
