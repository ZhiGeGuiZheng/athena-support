package com.opdar.athena.support;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.DispatcherServlet;
import com.opdar.platform.core.base.GulosityServer;
import com.opdar.platform.core.base.ServletEventListener;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shiju on 2017/1/23.
 */
public class MainServer {
    public static Map<String, String> valueOf(String[] args) {
        Map<String,String> map = new HashMap<String, String>();
        for(int i=0;i<args.length;i+=2){
            String s = args[i];
            map.put(s,args[i+1]);
        }
        return map;
    }
    public static void main(String[] args) {
        Map<String, String> commands = valueOf(args);
        int port = 18182;
        if(commands.containsKey("--port")){
            String _port = commands.get("--port");
            if(!StringUtils.isEmpty(_port)){
                port = Integer.valueOf(_port);
            }
        }
        Context.putResourceMapping("/css","classpath:/static/css");
        Context.putResourceMapping("/fonts","classpath:/static/fonts");
        Context.putResourceMapping("/imgs","classpath:/static/imgs");
        Context.putResourceMapping("/js","classpath:/static/js");

        Server server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(1);
        context.addEventListener(new ServletEventListener());
        context.setContextPath("/");
        context.addServlet(DispatcherServlet.class, "/");
        server.setHandler(context);
        try {
            server.start();
            server.join();
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }
}
