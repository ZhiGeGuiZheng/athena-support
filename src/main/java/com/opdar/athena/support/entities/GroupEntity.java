package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.GroupMapper;
import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.plugins.mybatis.annotations.Id;
import com.opdar.plugins.mybatis.annotations.Namespace;
import com.opdar.plugins.mybatis.annotations.Sort;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/8/2.
 */
@Namespace(GroupMapper.class)
public class GroupEntity {
    @Id
    private String id;
    private String name;
    private String appId;
    private String creatorId;
    @Sort(type = Sort.SortType.DESC)
    private Timestamp createTime;
    private Timestamp updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
