<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <#assign title="登陆"/>
<#include "resource.ftl"/>
</head>

<body>
<div style="width: 100%;">
    <div class="login-form">
        <form class="form-signin" style="padding-top: 200px;">
            <div class="form-signin-heading">欢迎使用Athena客服 </div>
            <br/>
            <div class="form-inputs" >
                <input type="text" name="userName"  v-model="userName" class="input-default" placeholder="请输入账号" required="" autofocus="">
                <br/>
                <input type="password" name="userPwd" v-model="userPwd" class="input-default" placeholder="请输入密码" required="">
                <br/>
                <button class="btn btn-lg btn-primary btn-block" style="margin-top:6px" type="submit" >登入</button>
                <div style="font-size: 12px;margin-top: 8px;">还没有账号？<a style="color: #0cbb00;"  :href="'register'+location.href.substr(location.href.indexOf('?'))">注册</a>或<a style="color: #0cbb00;" :href="'anonymous'+location.href.substr(location.href.indexOf('?'))">匿名会话</a></div>
            </div>
        </form>
    </div>
</div>
<script>

    var vm = new Vue({
        el: '.login-form',
        data: {
            userName: '',
            userPwd: ''
        }
    });
</script>
</body>
</html>
